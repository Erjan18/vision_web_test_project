from django.shortcuts import render
from .models import *
# Create your views here.

def landing_page(request):
    landings = Landing.objects.all()
    blocks = Block.objects.all()
    context = {"landings":landings,'blocks':blocks}
    return render(request,'landing_test.html',context)

