from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse
from .models import *
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate


class ProfileTestCase(APITestCase):
    def setUp(self):
        self.create_url = reverse('register')
        self.profile = Profile.objects.create(name='erjan',
                               last_name='karimov',
                               age='18',
                               birth_date='2002-11-29',
                               city='Bishkek',
                               phone='0708324833',)

    def test_profile_create(self):
        data = {
                "username": "erlan",
                "email": "zumc4847@gmail.com",
                "password": "qwertij1215",
                "profile": {
                    "name": "саша",
                    "last_name": "Кaримов",
                    "age": "19",
                    "birth_date": "2001-12-29",
                    "city": "Kant",
                    "phone": "0708243166"
                }
            }
        self.response = self.client.post(self.create_url, data,format='json')
        print(self.response.json())
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    # def test_profile_create_empty_username(self):
    #     data =  {
    #             "username": "",
    #             "email": "zumc4847@gmail.com",
    #             "password": "qwertij1215",
    #             "profile": {
    #                 "name": "саша",
    #                 "last_name": "Кaримов",
    #                 "age": "19",
    #                 "birth_date": "2001-12-29",
    #                 "city": "Kant",
    #                 "phone": "0708243166"
    #             }
    #         }
    #     self.response = self.client.post(self.create_url, data,format='json')
    #     print(self.response.json())
    #     self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
    #
    # def test_profile_empty_name_and_lastname(self):
    #     data = {
    #             "username": "",
    #             "email": "zumc4847@gmail.com",
    #             "password": "qwertij1215",
    #             "profile": {
    #                 "name": "",
    #                 "last_name": "",
    #                 "age": "19",
    #                 "birth_date": "2001-12-29",
    #                 "city": "Kant",
    #                 "phone": "0708243166"
    #             }
    #         }
    #     self.response = self.client.post(self.create_url, data)
    #     self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
    #     print(self.response.data)
    #
    # def test_profile_empty_email(self):
    #     data =  {
    #             "username": "erlan",
    #             "email": "",
    #             "password": "qwertij1215",
    #             "profile": {
    #                 "name": "саша",
    #                 "last_name": "Кaримов",
    #                 "age": "19",
    #                 "birth_date": "2001-12-29",
    #                 "city": "Kant",
    #                 "phone": "0708243166"
    #             }
    #         }
    #     self.response = self.client.post(self.create_url, data, format='json')
    #     print(self.response.json())
    #     self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
