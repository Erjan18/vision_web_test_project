from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField()
    birth_date = models.DateField()
    city = models.CharField(max_length=50)
    phone = models.IntegerField()
    user = models.OneToOneField(User,on_delete=models.SET_NULL,null=True)


    def __str__(self):
        return self.name
